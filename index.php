<?php
include 'db.php';
$projectRoot = __DIR__;
$projectRoot = str_replace('\\', '/', $projectRoot);
include_once $projectRoot . '/dao/DocumentDao.php';

$docDao = new DocumentDao($dbcon);
$xml = simplexml_load_file('data/order_6270380.xml');

$allowToSave = true;

$messages = [];

if (!$docDao->checkDocEmpty($xml)) {
    $allowToSave = false;
    $messages[] = "Field is empty";
}

if (!$docDao->checkDocUniq($xml->NUMBER)) {
    $allowToSave = false;
    $messages[] = "Document is exist";
}

if ($allowToSave) {
    $docDao->saveDocument($xml);
    $messages[] = "Saved";
}


foreach ($messages as $msg) {
    echo "$msg |";
}



