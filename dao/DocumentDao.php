<?php


class DocumentDao
{
    var $dbcon;

    function DocumentDao($dbcon)
    {
        $this->dbcon = $dbcon;
        $this->dbcon->query('SET SQL_BIG_SELECTS=1');
    }

    function saveDocument($xml)
    {
        $docNum = $xml->NUMBER;
        $date = $xml->DATE;
        $deliveryDate = $xml->DELIVERYDATE;
        $sender = $xml->HEAD->SENDER;
        $recipient = $xml->HEAD->RECIPIENT;

        $sql = "insert into documents 
                (docNum, date, deliveryDate, sender, recipient, sendTimestamp) 
              values 
                ('$docNum', '$date', '$deliveryDate', '$sender', '$recipient', CURRENT_TIMESTAMP())";
        mysqli_query($this->dbcon, $sql);

        $id = mysqli_insert_id($this->dbcon);
        $body = serialize(json_decode(json_encode($xml), true));

        $sql = "insert into documents_body (docId, body) values ($id, '$body')";

        mysqli_query($this->dbcon, $sql);
    }

    function checkDocUniq($number)
    {
        $sql = "select count(*) as cnt from documents WHERE docNum='$number'";
        $result = mysqli_query($this->dbcon, $sql);
        $cnt = mysqli_fetch_array($result, MYSQLI_ASSOC);

        return $cnt['cnt'] == 0;
    }

    function checkDocEmpty($xml)
    {
        if ($xml->NUMBER == null || $xml->NUMBER == '') return false;
        if ($xml->DATE == null || $xml->DATE == '') return false;
        if ($xml->DELIVERYDATE == null || $xml->DELIVERYDATE == '') return false;
        if ($xml->HEAD->SENDER == null || $xml->HEAD->SENDER == '') return false;
        if ($xml->HEAD->RECIPIENT == null || $xml->HEAD->RECIPIENT == '') return false;
        return true;
    }
}